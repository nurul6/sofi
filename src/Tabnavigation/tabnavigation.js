import {View, Text} from 'react-native';
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicions from "react-native-vector-icons/Ionicons";


const Tab = createBottomTabNavigator();

import Home from '../pages/home';
import Setting from '../pages/kategori';
import Menu from '../pages/saya';


export default function Tabnavigation() {
    return (
        
        <Tab.Navigator screenOptions={{
            scrollEnabled: true,
            tabBarActiveTintColor: 'black',
            tabBarActiveBackgroundColor : 'silver',
        }
    }>
          <Tab.Screen name="home" component={Home}
        options={{
        tabBarIcon: () => {
            return <Ionicions name="home" style={{color: 'black', fontSize:25}}/>;
        },
        }}/>
        <Tab.Screen name="kategori" component={Menu}
        options={{
        tabBarIcon: () => {
        return <Ionicions name="grid" style={{color: 'black', fontSize:25}}/>;
        },
    }}/>
      <Tab.Screen name="saya" component={Setting}
        options={{
            tabBarIcon: () => {
                return <Ionicions name="person-outline" style={{color:'black', fontSize:25}}/>;
            },
        }}
        />
        </Tab.Navigator>
    );
}


