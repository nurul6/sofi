import React, {component} from "react";
import {text, view, StyleSheet, Touchableopacity, ImageBackground, Image } from 'react-native';

const Tombol = ({label, onPress}) => {
    return(
        <TouchableOpacity onPress={onPress} style={styles.container}>
            <Text>{label}</Text>
        </TouchableOpacity>
    );
};

export class home extends component {
    render (){
        return(
            <View style={{flex:1}}>
                <View style={{flex:1}}>
                    <ImageBackground source={require('../assets/gambar.jpeg')} style={{width:360, height:209}}></ImageBackground>
                </View>
            </View>
        );
    };
}

export default Home;

const styles = StyleSheet.create({
    container : {
        padding : 7,
        margin : 5,
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : ''
    },
    page :{
        height: 900,
        backgroundColor:''
    }
})
