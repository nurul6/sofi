import React, {Component} from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';

const Tombol = ({label, onPress}) => {
    return(
        <TouchableOpacity onPress={onPress} style={styles.container}>
            <Text>{label}</Text>
        </TouchableOpacity>
    );
};

export class Menu extends Component {
    render (){
        return(
            <ScrollView style={styles.page}>
                <Text style={styles.welcomeText}>Kategori</Text>
                <TouchableOpacity style={{borderRadius:100,padding:30,margin:90,marginVertical:10,alignItems:'center',textalign:'center', backgroundColor:'silver'}}>
                    <Text style={{fontSize:25,}}>kaktus</Text>
                </TouchableOpacity>
            </ScrollView>
            
        );
    };
}
export default Menu;